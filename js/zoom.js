/**
 * Zoom class
 */
class Zoom {
    mainScale = 2;
    scale = 1;
    canvas = null;
    options = {};
    constructor(canvas, options) {
        this.canvas = canvas;
        this.options = options;
        this.scale = window.devicePixelRatio;
        this.#init();
    }

    /**
     * Returns an option value by given key
     * @param key
     * @param def
     * @returns {*}
     */
    getOption(key, def) {
        if (typeof this.options[key] != 'undefined') {
            return this.options[key];
        }
        return def;
    }

    /**
     * Initializes the instance
     * @returns {Zoom}
     */
    #init() {
        this.canvas.style.width = this.canvas.width + 'px';
        this.canvas.style.height = this.canvas.height + 'px';
        this.canvas.width = this.canvas.width * this.scale;
        this.canvas.height = this.canvas.height * this.scale;
        /** @type {CanvasRenderingContext2D} */
        this.ctx = this.canvas.getContext('2d');
        this.ctx.scale(this.scale, this.scale);
        this.ctx.translate(this.canvas.width / 2 / this.scale, this.canvas.height / 2 / this.scale);
        this.canvas.addEventListener('wheel', ((e) => {
            const s = (e.deltaY < 0 || e.wheelDelta > 0) ? 1.0 / 1.1 : 1.1;
            this.mainScale *= s;
            return new Promise((() => {
                this.render(0, 0);
            }).bind(this));
        }).bind(this));
        return this;
    }

    /**
     * Resets the zoom to the default position
     */
    reset() {
        this.scale = window.devicePixelRatio;
        this.mainScale = 2;
        this.render(0, 0);
    }

    /**
     * Restores the canvas
     */
    #restore() {
        this.ctx.save()
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.restore();
    }

    /**
     * Draws a line by given parameters
     * @param startX
     * @param startY
     * @param endX
     * @param endY
     * @param options
     */
    #line(startX, startY, endX, endY, options) {
        this.ctx.beginPath();
        this.ctx.setLineDash(options.style)
        this.ctx.lineDashOffset = 5;
        this.ctx.strokeStyle = this.getOption('color', '#000000');
        this.ctx.moveTo(startX, startY);
        this.ctx.lineTo(endX, endY);
        if (typeof options.label != 'undefined' && this.getOption('labels', 0)) {
            if (parseFloat(options.label.text) < 1 && parseFloat(options.label.text) > -1) {
                this.ctx.strokeText(parseFloat(options.label.text).toPrecision(2), options.label.x, options.label.y);
            } else {
                this.ctx.strokeText(options.label.text, options.label.x, options.label.y);
            }
        }
        this.ctx.closePath();
        this.ctx.stroke();
    }

    /**
     * Renders the grid.
     * @param x
     * @param y
     */
    render(x, y) {
        this.#restore();
        this.ctx.translate(x, y);
        const step = Math.pow(5.0, Math.floor(Math.log(this.mainScale) / Math.log(5.0)));
        const transform = this.ctx.getTransform();
        const cW = this.canvas.width / this.scale;
        const cH = this.canvas.height / this.scale;
        const cX = (cW / 2 - transform.e);
        const cY = (cH / 2 - transform.f);
        const absCX = Math.abs(cX);
        const absCY = Math.abs(cY);
        const lineStartC = -cW - absCY;
        const lineEndC = cW + absCY;
        /**
         * Vertical rows
         */
        for (let column = -10; column <= 10; column += 1) {
            const pos = column * step;
            const ix = 300 * (pos / this.mainScale);
            this.#line(ix, lineStartC, ix, lineEndC, {label: {text: pos, x: ix, y: 0}, style: []});

            /**
             * Vertical dotted rows
             */
            for (let column = 1; column < 5; column += 1) {
                const pos = column * step;
                let ixDotted = ix + ((300 * pos / 5 / this.mainScale))
                this.#line(ixDotted, lineStartC, ixDotted, lineEndC, {style: [2,2]});
            }
        }
        /**
         * Horizontal rows
         */
        const lineStartR = -cW - absCX;
        const lineEndR = cW + absCX;
        for (let row = -10; row <= 10; row += 1) {
            const pos = row * step;
            const iy = 300 * (pos / this.mainScale);
            this.#line(lineStartR, iy, lineEndR, iy, {label: {text: pos, x: 0, y: iy}, style: []});
            /**
             * Horizontal dotted rows
             */
            for (let row = 1; row < 5; row += 1) {
                const pos = row * step;
                let iyDotted = iy + ((300 * pos / 5 / this.mainScale))
                this.#line(lineStartR, iyDotted, lineEndR, iyDotted, {style: [2,2]});
            }
        }
    }
}
