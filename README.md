# tfeiszt zoom

### Description
ES5 test application for a simulation of "infinite" zoom

[Screenshot](screenshot.png)

### Browser compatibility
* IE9
* IE10
* IE11
* Firefox
* Safari
* Opera
* Chrome
* Edge

### License

MIT
